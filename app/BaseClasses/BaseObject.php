<?php

namespace App\BaseClasses;

use Illuminate\Database\Eloquent\Model;

abstract class BaseObject
{
  protected $dataSourceUrl;
  protected $limit = null;
  protected $offset = 0;

  public function __construct(string $dataSource, int $limit = null, int $offset = null){
    $this->dataSourceUrl = $dataSource;
    $this->limit = $limit;
    $this->offset = $offset;
  }

  protected function all(){
     $res = file_get_contents(base_path('resources/data_source/' . $this->dataSourceUrl));
     $res = json_decode($res, true);

     // offset is at least 0 in case that the parameter is null.
     // In case that limit be null, array slice dont care about it
     return array_slice($res, $this->offset, $this->limit);
  }

  protected function find($key, $value){
    $res = $this->all();
    foreach ($res as $r) {
      if ($r[$key] == $value){
        return $r;
      }
    }
    return null;
  }
}
