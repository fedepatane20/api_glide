<?php

namespace App\BaseClasses;
use App\Services\ApiConnector;

class Employee extends BaseObject
{
  public function all(){
    $api = new ApiConnector();
    return array_slice($api->post($this->dataSourceUrl), $this->offset, $this->limit);
  }
  public function find($key, $value){
    return parent::find($key, $value);
  }
}
