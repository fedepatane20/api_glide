<?php

namespace App\BaseClasses;

use Illuminate\Database\Eloquent\Model;

class Office extends BaseObject
{
  public function all(){
    return parent::all();
  }
  public function find($key, $value){
    return parent::find($key, $value);
  }
}
