<?php

namespace App;
use App\Expander;

class ExpandGetter
{
   public function __construct(){

   }

   public function get($query){
     $query  = explode('&', $query);

     $params = array();
     foreach($query as $param)
     {
         list($name, $value) = explode('=', $param);
         $params[urldecode($name)][] = urldecode($value);
     }
     if (isset($params["expand"])){
       return $params["expand"];
     }
     else{
       return null;
     }
   }
}
