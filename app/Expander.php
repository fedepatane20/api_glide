<?php

namespace App;
use App\Factory;

class Expander
{
   public function __construct(){

   }

   public static function expand(array $object, array $expand){
     try{
       if (count($expand) == 0)
       {
         return $object;
       }

       // if the object contains the expand type , and is not null
       if (in_array($expand[0], array_keys($object)) && isset($object[$expand[0]])){
        // get the controller given the type
        $abstraction  = Factory::getAbstraction($expand[0], $object[$expand[0]]);

        // use polimorfhism to get the value , given the id of the model
        $value = $abstraction->show_app($object[$expand[0]]);

        // use the value to call recursively the function
        $object[$expand[0]] = self::expand($value, array_slice($expand, 1) );
       }
     }
     catch(Exception $e){
       return false;
     }

     return $object;
   }
}
