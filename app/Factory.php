<?php

namespace App;
use App\Http\Controllers\DepartmentsController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\OfficesController;

class Factory
{
   public function __construct(){

   }

   public static function getAbstraction(string $type){
     switch ($type) {
         case 'manager':
            return new EmployeesController();
         case 'office':
            return new OfficesController();
         case 'department':
            return new DepartmentsController();
         case 'superdepartment':
            return new DepartmentsController();
     }
   }
}
