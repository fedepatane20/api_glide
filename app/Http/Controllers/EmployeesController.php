<?php

namespace App\Http\Controllers;

use App\BaseClasses\Employee;
use App\InfoGetter;
use App\InputGetter;
use Illuminate\Http\Request;


class EmployeesController extends Controller
{
    public function index(Request $req)
    {
      try {

        $inputGetter = new InputGetter();
        $inputs = $inputGetter->get($_SERVER['QUERY_STRING'], $req);
        $limit = $inputs["limit"];
        $offset = $inputs["offset"];
        $expands = $inputs["expands"];


        $obj = new Employee('https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees', $limit, $offset);
        $data = new InfoGetter();
        return response()->json($data->get($obj, $expands), 200);


      }
      catch(Exception $e){
        return response()->json(['errors' => $e->getMessage()], 500);
      }
    }

    public function show(Request $req, $id)
    {
      try {
        $obj = new Employee('https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees', null, 0);
        return response()->json([ $obj->find("id", $id)], 200);

      }
      catch(Exception $e){
        return response()->json(['errors' => $e->getMessage()], 500);
      }

    }

    public function show_app($id)
    {
        $obj = new Employee('https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com/bigcorp/employees', null, 0);
        return $obj->find("id", $id);
    }
}
