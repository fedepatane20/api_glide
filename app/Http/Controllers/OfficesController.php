<?php

namespace App\Http\Controllers;

use App\BaseClasses\Office;
use App\InfoGetter;
use App\InputGetter;
use Illuminate\Http\Request;

class OfficesController extends Controller
{

    public function index(Request $req)
    {
      try{

        $inputGetter = new InputGetter();
        $inputs = $inputGetter->get($_SERVER['QUERY_STRING'], $req);
        $limit = $inputs["limit"];
        $offset = $inputs["offset"];
        $expands = $inputs["expands"];

        $office = new Office('offices.json', $limit, $offset);
        $data = new InfoGetter();

        return response()->json($data->get($office, $expands), 200);
      }
      catch(Exception $e){
        return response()->json(['errors' => $e->getMessage()], 500);
      }

    }

    public function show(Request $req, $id)
    {
      try{
        $obj = new Office('offices.json', null, 0);
        return response()->json($obj->find("id", $id), 200);
      }
      catch(Exception $e){
        return response()->json(['errors' => $e->getMessage()], 500);
      }

    }

    public function show_app($id)
    {
        $obj = new Office('offices.json', null, 0);
        return $obj->find("id", $id);
    }


}
