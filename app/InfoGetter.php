<?php

namespace App;
use App\Expander;

class InfoGetter
{
   public function __construct(){

   }

   public function get($object, array $expands = null){
     $res = $object->all();
     if (isset($expands)){
       foreach ($res as $key => $r) {
         foreach ($expands as $expand) {
           $res[$key] = Expander::expand($r , $expand);
           $r = $res[$key];
         }
       }
     }
     return $res;
   }
}
