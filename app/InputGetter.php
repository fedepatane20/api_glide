<?php

namespace App;
use App\ExpandGetter;
use Illuminate\Http\Request;

class InputGetter
{
   public function __construct(){

   }

   public function get($server, $req){
     // I have to create this class, because laravel cant support repeated parameters in the url.
     $exp = new ExpandGetter();
     $expands = $exp->get($server);

     $offset = $req->input('offset');
     $limit = $req->input('limit');

     if (isset($expands)){
       foreach ($expands as $key => $value) {
         $expands[$key] = explode(".", $value);
       }
     }

     if (!isset($offset)){
       $offset = 0;
     }

     return array("limit" => $limit, "offset" => $offset, "expands" => $expands);
   }
}
