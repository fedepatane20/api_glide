<?php

namespace App\Services;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client;

class ApiConnector
{
    function __construct()
    {

    }

    function post($uri, $extra_headers=array(), $data=array())
    {
        try {
          $client = new Client();
          $params['headers'] = ['Content-Type' => 'application/json'];
          $response = $client->post($uri, $params);
          return json_decode($response->getBody()->getContents(), true);
        }
        catch (RequestException $e)
        {
            Log::error('Request ERROR: '. $e);

            return $response;
        }
    }
}
